import Log from '../src/log';
import View from '../src/view';
import DOM from '../src/dom';
import Emmet from '../src/emmet';
import Utils from '../src/utils';



var mapStyles = [{"featureType":"all","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"},{"weight":"1"},{"saturation":"0"},{"hue":"#00ffce"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#edede8"},{"saturation":"0"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"simplified"},{"color":"#767667"},{"saturation":"0"}]},{"featureType":"poi","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"},{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"color":"#efebe2"},{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"},{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"simplified"},{"saturation":"0"},{"color":"#bbd098"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"simplified"},{"color":"#767676"},{"saturation":"0"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"},{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"},{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"color":"#ebebeb"},{"saturation":"0"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#fbfbfb"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#b6d7e6"}]}];

var StoreLocator = {
	map: null,
	markers: [],
	stores: null,
	ownStoresMarkers: [],

	// clientScore to define distance advance compared to dealers
	clientScore: 5, // ~ 5 km by air

	//shownClosestIds: [],
	shownMarkerId: 0,
	shownMarkerIds: [],
	closestStoreShown: false,
	markerStoreShown: false,
	geoPos: false, // if user accepted geolocation
	startPos: null,
	updatedPos: null,
	mobile: false,
	distances: [],
	distancesCollection: [],
	noMarkersTimeout: null,
	delayedSetMapCenterObj: null,
	onlyClientShops: false,

	searchTerm: '',
	searchMarker: null,
	searchNearestStoreLatLng: null,
	searchInput: null,
	searchButton: null,
	maxSearchResults: 10,
	resultsCon: null,
	resultsConInner: null,
	noResultsCon: null,
	noResultsContent: "",
	storeTmpl: '',
	openingHourTmpl: '',
	loadMoreTmpl: '',
	autocomplete: null,

	storeSidebar: null,
	loader: null,
	defaultDescriptionCon: null,
	
	init: function(){
		Log.fn("StoreLocator | init")
		var self = this;

		var maps_script = document.createElement('script');
		maps_script.setAttribute('src', window.location.protocol + SitePreferences.GOOGLE_MAPS_API_KEY);
		document.head.appendChild(maps_script);

		self.storeSidebar = $('.store-locator-nav');
		self.loader = self.storeSidebar.find('.loader');

		var searchCon = $('.store-locator-search');
		self.searchButton = searchCon.find('.button-search');
		self.searchInput = searchCon.find('input').focus();

		self.forceClosestBtn = $('.force-closest');

		self.resultsCon = self.storeSidebar.find(".store-locator__search-results");
		self.resultsConInner = self.resultsCon.find(".inner");
		self.noResultsCon = self.resultsCon.find(".no-results");
		self.noResultsContent = self.noResultsCon.html();
		self.storeTmpl = self.storeSidebar.find('.store-tmpl').html();
		self.openingHourTmpl = self.storeSidebar.find('.opening-hour-tmpl').html();
		self.loadMoreTmpl = self.storeSidebar.find('.load-more-tmpl').html();
		//self.closestCon = self.storeSidebar.find('.closest-store-con');
		self.markerStoreCon = self.storeSidebar.find(".marker-store-con");
		self.defaultDescriptionCon = self.storeSidebar.find(".default-desc");

		if (typeof window.matchMedia != "undefined") {
			if(window.matchMedia("(max-width: 767px)").matches){
				self.mobile = true;
				self.searchInput.attr('placeholder', 'Søg');
			}
		}

		window.initClientMap = function(){

			Log.fn("window | initClientMap")
			var clientMapType = new google.maps.StyledMapType(mapStyles);

			var clientMapTypeId = 'client_style';
			self.map = new google.maps.Map(document.getElementById('map-container'), {
				zoom: 7,
				center: {lat: 55.88, lng: 11.72}, // show denmark
				mapTypeControl: false
			});

			self.map.mapTypes.set(clientMapTypeId, clientMapType);
			self.map.setMapTypeId(clientMapTypeId);

			var location = window.location.hostname;
			if(location.indexOf('.dk') > 0){
				self.map.setZoom(7); // denmark
				self.map.setCenter(new google.maps.LatLng(55.88, 11.12));
			} else if(location.indexOf('.se') > 0){
				self.map.setZoom(5); // sweden
				self.map.setCenter(new google.maps.LatLng(62.88, 15.12));
			} else {
				self.map.setZoom(4); // europe
				self.map.setCenter(new google.maps.LatLng(55.88, 11.12));
			}

			self.getStores();
			self.getGeoLocation();
			self.resetSearch();
			self.bindClicks();
								
			var autoOptions = null;
						
			var geoLoc = $('#geoLoc').data();
			if (geoLoc != null){
				if (geoLoc.latitude != null && geoLoc.longitude != null){
					//console.log("geoLoc");
					autoOptions = {
							bounds: new google.maps.LatLngBounds(
									new google.maps.LatLng({lat: geoLoc.latitude, lng: geoLoc.longitude}),
									new google.maps.LatLng({lat: geoLoc.latitude, lng: geoLoc.longitude})),
							types: ['geocode']
					};
					Log.db("bounds",{lat: geoLoc.latitude, lng: geoLoc.longitude})
				}
			}
			
			if(autoOptions == null){
				self.forceGeoLookup()
				if (self.startPos != null){
					if (self.startPos.coords != null){
						if (self.startPos.coords.latitude != null && self.startPos.coords.longitude != null){
							//console.log("GeoLookup");
							autoOptions = {
									bounds: new google.maps.LatLngBounds(
											new google.maps.LatLng({lat: self.startPos.coords.latitude, lng: self.startPos.coords.longitude}),
											new google.maps.LatLng({lat: self.startPos.coords.latitude, lng: self.startPos.coords.longitude})),
									types: ['geocode']
							};
						}
					}
				}
			}
			var country = $('#country').data().country;
			if(autoOptions == null){
				//console.log("country");
				autoOptions = {
						componentRestrictions : {country: country},
						types: ['geocode']
				};
				
			}
							
			var autocomplete = new google.maps.places.Autocomplete(self.searchInput[0], autoOptions);
			autocomplete.addListener('place_changed', function(e) {
				var place = autocomplete.getPlace();
				var tmpTerm = self.searchInput.val();
				self.searchButtonClick(tmpTerm, e);
			});

			Log.fn("window | initClientMap done")
		}
	},
	formatDirectionsUrl: function(data){
		var self = this;
		if(self.searchTerm != ''){
			return 'https://maps.google.com?saddr=' + encodeURIComponent(self.searchTerm) + '&daddr=' + data.AdressLine2 + ',' + data.AreaCode + ',' + data.City + ',' + data.Country;
		} else {
			return 'https://maps.google.com?saddr=' + (this.geoPos ? this.startPos.coords.latitude + ',' + this.startPos.coords.longitude : 'My+Location') + '&daddr=' + data.AdressLine2 + ',' + data.AreaCode + ',' + data.City + ',' + data.Country;
		}
	},
	formatShopUrl: function(url){
		var urlElem = "";
		if(url != null){
			urlElem = '<a href="http://' + url + '">' + url + '</a>'; 
		}
		return urlElem;
	},
	hideDefaultDesc: function(){
		this.defaultDescriptionCon.removeClass('shown');
	},
	showLoader: function(){
		this.loader.addClass('loading');
	},
	hideLoader: function(){
		this.loader.removeClass('loading');
	},
	bindClicks: function(){ // binding click and "enter"-events to search input, -button and checkbox.
		Log.fn("StoreLocator | bindClicks")

		var self = this;
		self.storeSidebar.find('.client-filter-con input').off('click').click(function(e){
			var checked = $(this).prop('checked');
			self.onlyClientShops = (checked == true ? true : false);
			self.reformatStores(self.onlyClientShops == true ? "client" : "all");
		});
		self.searchButton.off('click').click(function(e){
			e.preventDefault();
			var term = self.searchInput.val();
			self.searchButtonClick(term, e);
		});
		self.forceClosestBtn.off('click').click(function(e){
			e.preventDefault();
			self.resetSearch();
			self.forceGeoLookup();
		});
	},
	bindSidebarLocators: function(){
		var self = this;
		self.storeSidebar.find('.result').off('click').click(function(e){
			e.preventDefault();
			var id = $(this).attr('data-store-id');
			self.newLocation(id, e);
		});
		self.storeSidebar.find('.result .locator').off('click').click(function(e){ // .locator only visible on mobile
			e.preventDefault();
			e.stopPropagation();
			var id = $(this).attr('data-store-id');
			self.newLocation(id, e);
			$(window).scrollTop($('#primary').offset().top - 80);
		});
		self.storeSidebar.find('.result .directions').off('click').click(function(e){
			window.open($(this).attr('href'), '_blank');
		});
		self.storeSidebar.find('.result .storeurl').off('click').click(function(e){
			window.open($(this).children('a').attr('href'), '_blank');
		});
	},
	bindSidebarOpeningHours: function(){
		var self = this;
		var activeOpeningHours = self.storeSidebar.find('.opening-hours.has-opening-hours');
		activeOpeningHours.each(function(){
			$(this).find('a.headline').off('click').click(function(e){
				e.preventDefault();
				var ohCon = $(this).closest('.opening-hours');
				if(ohCon.hasClass('loaded')){
					if(ohCon.hasClass('shown')){
						ohCon.removeClass('shown');
					} else {
						ohCon.addClass('shown');
					}

					self.bindSidebarOpeningHours();
				} else {
					var parent = $(this).closest('.result');
					self.openOpeningHours(parent, e);
				}
			});
		});
	},
	bindSidebarOpeningHoursNav: function(ohCon){
		var elm = ohCon,
		left = elm.find('.nav-left'),
		right = elm.find('.nav-right'),
		slider = elm.find('.slider'),
		slides = slider.find('.item'),
		slideCount = slides.length,
		activeSlide = 1;

		slider.css({display: 'block', width: "" + (100 * slideCount) + "%"});
		slides.each(function(elm, idx){
			$(this).css({width: (100 / slideCount) + "%"});
		});

		left.off('click').click(function(e){
			e.preventDefault();
			if(activeSlide > 1){
				activeSlide--;
				right.removeClass('disabled');
				slider.css({marginLeft: -(100 * (activeSlide - 1)) + "%"});
				if(activeSlide == 1){
					$(this).addClass('disabled');
				}
			}
		});
		right.off('click').click(function(e){
			e.preventDefault();
			if(activeSlide < slideCount){
				activeSlide++;
				left.removeClass('disabled');
				slider.css({marginLeft: -(100 * (activeSlide - 1)) + "%"});
				if(activeSlide == slideCount){
					$(this).addClass('disabled');
				}
			}
		});
		elm.find('.storeslist').off('click').click(function(e){
			window.location.href=$(this).attr('href');
		});
	},


	// ---------------------------------------------------------------------
	// Everything with Client stores for the map
	// ---------------------------------------------------------------------
	getStores: function(){
		Log.fn("StoreLocator | getStores")
		var self = this;

		var getStores = $.ajax({
			//url: "/on/demandware.store/Sites-client-Site/default/Stores-FindJson",
			url: 'tmp-stores.json',
			contentType: "application/json; charset=utf-8",
            type: "GET",
            dataType: "json",
            success: function (data, status, e)
            {
                if (data.length > 0) {
                	//self.formatOpeningHours(elm, loader, data);
                	self.stores = data;
					self.formatStores(self.stores);
                } else {
                	self.loader.removeClass('loading');
                	// TODO - error messages
                    //console.log('fail data: ', data);
	                //console.log('fail status: ', status);
	                //console.log('fail e: ', e);
	                //self.serviceFailed();
                }
            },
            error: function (data, e, stack)
            {
            	self.loader.removeClass('loading');
            	// TODO - error messages
                //console.log('fail data: ', data);
                //console.log('fail e: ', e);
                //console.log('fail stack: ', stack);
	            //self.serviceFailed();
            }
		});
		
	},
	scrollToResult: function(id){
		var self = this,
			parent = self.storeSidebar.find('.result[data-store-id='+ id +']');

		if(parent.length == 1){
			var topPos = parent[0].offsetTop;
			self.storeSidebar.find('.content .store-locator__search-results .inner').scrollTop(topPos);
			self.storeSidebar.find('.result').removeClass('selected');
			parent.addClass('selected');
		} else {
			self.displaySidebarMarker(id, '');
		}
	},
	scrollToResultMobile: function(id){
		var self = this,
			parent = self.storeSidebar.find('.result[data-store-id='+ id +']');

		if(parent.length == 1){
			var topPos = parent.offset().top;
			$(window).scrollTop(topPos);
		} else {
			self.displaySidebarMarker(id, '');
		}
	},
	formatStores: function(data){
		Log.fn("StoreLocator | formatStores ")
		var self = this;

		for (var i = 0; i < data.length; i++) {

			if(data[i].Expired !== true && data[i].Hidden !== true){
				var pos = new google.maps.LatLng(data[i].Latitude, data[i].Longitude);

				//debugger;
				var markerIcon = new google.maps.MarkerImage(
					(data[i].OwnShop ? StoreLocatorIcons.mapMarkerFlagship2x : StoreLocatorIcons.mapMarkerStore2x ), null, null, null, new google.maps.Size(25,32));
				var marker = new google.maps.Marker({
					position: pos,
					map: self.map,
					icon: markerIcon,
					clickable: true,
					id: data[i].Id,
					Id: data[i].Id,
					OwnShop: data[i].OwnShop
				});

				var content = '<div style="text-align: center;">';
				content += (data[i].OwnShop == true ? '<img width="120" src="' + StoreLocatorIcons.clientLogoSimple2x + '"/><br /><br />' : '');
				content += '<span style="text-transform: uppercase;">' + (data[i].OwnShop == true ? Resources.STORE_LOCATOR_MASAISTORE : Resources.STORE_LOCATOR_RETAILER) + '</span><br />';
				content += "<strong style='font-weight:bold;'>" + data[i].AdressLine1 + "</strong><br />";
				content += data[i].AdressLine2 + "<br />";
				content += data[i].AreaCode + " ";
				content += data[i].City + "<br />";
				content += Resources.STORE_LOCATOR_PHONE + data[i].Phone + "<br />";
				if(data[i].ShopURL != null){
					content += '<a href="http://' + data[i].ShopURL +  '" target="_blank">' + data[i].ShopURL + '</a><br /><br />';
				}
				
				content += '<a href="' + self.formatDirectionsUrl(data[i]) + '" target="_blank">' + Resources.STORE_LOCATOR_DIRECTIONS + '</a><br />';
				if(data[i].OpeningHours == true && data[i].OwnShop == true){
					content += '<a href="javascript:;" class="open-opening-hours" data-store-id="'+ data[i].Id +'">' + Resources.STORE_LOCATOR_OPENINGHOURS + '</a><br /><br />';
				}
				if(data[i].OwnShop == true){
					content += '<a href="' + Resources.STORE_LOCATOR_ALLSHOPSULR + '">' + Resources.STORE_LOCATOR_ALLSHOPS + '</a>';
				}
				content += '</div>';

				marker.info = new google.maps.InfoWindow({
					content: content
				});
				google.maps.event.addListener(marker, 'click', function(e) {
					for(var i = 0; i < self.markers.length; i++ ) {
				        self.markers[i].info.close();
				    }
					marker.info.setContent(this.info.content);
					marker.info.open(self.map, this);
					if(!self.mobile){
						self.scrollToResult(this.id);
					} else {
						self.displaySidebarMarker(this.id, '');
					}
					google.maps.event.addListener(marker.info, 'domready', function() {
						var ooh = $('.open-opening-hours');
						ooh.off('click').on('click', function(e) {
							e.preventDefault();
							var id = $(this).attr('data-store-id');
							self.openOpeningHoursFromMap(id);
						});
					});
				});
				google.maps.event.addListener(marker.info, 'closeclick', function(){
					self.hideMarkerStore();
				});
				if(data[i].OwnShop){
					self.ownStoresMarkers.push(data[i]);
				}
				self.markers.push(marker);
			}
		}

		Log.db("self.markers.length",self.markers.length)

		// if(window.MarkerClusterer){
		// 	self.markerCluster = new MarkerClusterer(self.map, self.markers, self.markerClusterOptions);
		// 	//this.markerCluster.addMarkers(self.markers);
		// 	//self.markerCluster.setIgnoreHidden(true);
		// }
	},
	reformatStores: function(type){ // reformatting markers to show only client stores or all
		var self = this;

		for(var i = 0; i < self.markers.length; i++) {
			if(self.markers[i].OwnShop == false){
				self.markers[i].setVisible((type == "all" ? true : false));
				// if(type == "all"){
				// 	self.markerCluster.addMarker(self.markers[i], true);
				// } else {
				// 	self.markerCluster.removeMarker(self.markers[i], true);
				// }
			} else {
				self.markers[i].setVisible(true);
			}
			self.markers[i].info.close(); // safety close otherwise markerClusterer chokes..
		}
		// self.markerCluster.resetViewport();
		// self.markerCluster.redraw();


		self.storeSidebar.find('.result[data-own-store="false"]').css('display', (type == "all" ? "block" : "none"));
		//console.log('self.storeSidebar.find(.result[data-own-store="false"]): ', self.storeSidebar.find('.result[data-own-store="false"]'));

		self.hideLoader();
	},




	// ---------------------------------------------------------------------
	// Geo Location
	// - If geo location cookie is present, that cookie will be used.
	// - Cookie expires after 20 minutes.
	// - Disapproved geo location will also be stored in the cookie for 20 minutes.
	// ---------------------------------------------------------------------
	getGeoLocation: function() {
		Log.fn("StoreLocator | getGeoLocation")
		var self = this;

		//var setGeoLocationUnavailableTimer = setTimeout(_(this.setGeoLocationUnavailable).bind(this), 5000);

		var geo = decodeURIComponent(Utils.cookie.get("GeoLocation"));
		if(geo !== "disapproved"){
			if(geo != "null" && geo != "undefined" && geo != "false"){
				var geoVar = (geo ? JSON.parse(geo) : "false");
				if(geoVar !== "false"){
					
					var position = {
						coords: {
							latitude: parseFloat(geoVar.latitude),
							longitude: parseFloat(geoVar.longitude)
						}
					}
					self.startPos = position;
					self.geoPos = true;
					self.hideLoader();
					self.hideDefaultDesc();
					self.handleGeoSuccess();
					
				} else {
					//self.doGeoLookup();
				}
			} else {
				//self.doGeoLookup();
			}
		} else {
			Utils.cookie.setMinutes("GeoLocation", 20, "disapproved", "/", false);
			self.hideLoader();
		}
	},
	doGeoLookup: function(){
		Log.fn("StoreLocator | doGeoLookup")
		var self = this;

		var cookie = Utils.cookie.get("GeoLocation");
		var geoCookie = (cookie != null && cookie != "undefined" ? cookie : "false");

		// if(geoCookie === "disapproved"){
		// 	Utils.cookie.setMinutes("GeoLocation", 20, "disapproved", "/", false);
		// 	self.hideLoader();
		// } else {
			if(geoCookie !== "false" || !JSON.parse(geoCookie)){
				var geoOptions = {
					enableHighAccuracy: true
				}
				//var setGeoLocationUnavailableTimer = setTimeout(_(this.setGeoLocationUnavailable).bind(this), 5000);

				var geoSuccess = function(position) {
					self.startPos = position;
					self.geoPos = true;
					self.hideLoader();
					Utils.cookie.setMinutes("GeoLocation", 20, '{"latitude": "'+ position.coords.latitude +'", "longitude":"'+ position.coords.longitude +'"}', "/", false);
					self.handleGeoSuccess();
				};
				var geoError = function(error) {
					if(error.code == 1){
						Utils.cookie.setMinutes("GeoLocation", 20, "disapproved", "/", false);
					} else {
						Utils.cookie.setMinutes("GeoLocation", 20, "false", "/", false);
					}
					self.hideLoader();
					
					//console.log('Geo-location status. Error code: ', error.code);
					// error.code can be:
					//   0: unknown error
					//   1: permission denied
					//   2: position unavailable (error response from location provider)
					//   3: timed out

				};
				var geo = JSON.parse(geoCookie);
				if(!geo.latitude){
					navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
				}
			} else {
				Utils.cookie.setMinutes("GeoLocation", 20, "false", "/", false);
			}
		//}
	},
	forceGeoLookup: function(){ // fired when "stores near me" is clicked.
		var self = this;
		self.showLoader();
		self.resetShownStores();
		self.resetSearch();
		Utils.cookie.setMinutes("GeoLocation", 20, "false", "/", false);

		var geoOptions = {
			enableHighAccuracy: true
		}
		//var setGeoLocationUnavailableTimer = setTimeout(_(this.setGeoLocationUnavailable).bind(this), 5000);

		var geoSuccess = function(position) {
			self.startPos = position;
			self.geoPos = true;
			self.hideLoader();
			Utils.cookie.setMinutes("GeoLocation", 20, '{"latitude": "'+ position.coords.latitude +'", "longitude":"'+ position.coords.longitude +'"}', "/", false);
			self.handleGeoSuccess();
		};
		var geoError = function(error) {
			//console.log('error code: ', error.code);
			if(error.code == 1){
				Utils.cookie.setMinutes("GeoLocation", 20, "disapproved", "/", false);
				$(".geo-disapproved-warning").show();
				$(".geo-disapproved-warning").click(function(e){
					$(this).hide();
				});
			} else {
				Utils.cookie.setMinutes("GeoLocation", 20, "false", "/", false);
			}
			
			self.hideLoader();
			
			//console.log('Geo-location status. Error code: ', error.code);
			// error.code can be:
			//   0: unknown error
			//   1: permission denied
			//   2: position unavailable (error response from location provider)
			//   3: timed out

		};
		//setTimeout(navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions) , 2000);
		navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
		
	},
	// setGeoLocationUnavailable: function(){
	// 	alert('GeoLocation not available.');
	// },
	handleGeoSuccess: function(){
		var self = this;

		if(self.markers === undefined || self.markers.length === 0 || self.map === undefined || self.map === null || self.geoPos === false){
			// if the stores ajax isn't completed and there's no markers yet
			if(self.noMarkersTimeout === null){
				self.noMarkersTimeout = window.setInterval(function(){self.handleGeoSuccess()}.bind(self), 500);
			}
		} else {
			window.clearInterval(self.noMarkersTimeout);
			self.noMarkersTimeout = false;

			self.findClosestMarker(self);

			var markerIcon = new google.maps.MarkerImage(StoreLocatorIcons.mapMarkerGps2x, null, null, null, new google.maps.Size(25,32));

			var marker = new google.maps.Marker({
			    position: new google.maps.LatLng(self.startPos.coords.latitude, self.startPos.coords.longitude),
			    map: self.map,
			    title: 'Din placering',
			    icon: markerIcon,
			    draggable: true,
			    client_id: "you"
			});

			var marker_dragend = google.maps.event.addListener(marker, 'dragend', function(marker){
				self.updatedPos = {
					coords: {
						latitude: marker.latLng.lat(),
						longitude: marker.latLng.lng()
					}
				};
				for(var i = 0; i < self.markers.length; i++) {
			        self.markers[i].info.close();
			    }
				self.findClosestMarker(self, {customEvent: "update", marker: marker});
			});
		}
	},
	rad: function(x){
		return x * Math.PI / 180;
	},
	showClientOnlyCheckbox: function(){
		var self = this,
			lbl = self.storeSidebar.find('.client-filter-con label');
		// self.onlyClientShops = false;
		// self.storeSidebar.find('.client-filter-con input').removeAttr('checked');
		lbl.show();
	},
	hideClientOnlyCheckbox: function(){
		var self = this,
			lbl = self.storeSidebar.find('.client-filter-con label');
		self.onlyClientShops = false;
		self.storeSidebar.find('.client-filter-con input').removeAttr('checked');
		lbl.hide();
	},
	findClosestMarker: function(findStoreObj) {
		var self = findStoreObj,
			foundClient = 0;

		if(self.startPos != null){
			self.onlyClientShops = false;
			$('.client-filter-con input').removeAttr('checked');
			var lat = self.startPos.coords.latitude;
		    var lng = self.startPos.coords.longitude;
			var customEvent = (arguments[1] ? (arguments[1].customEvent ? arguments[1].customEvent : undefined) : undefined);
			if(customEvent !== undefined && customEvent === "update"){
				var marker = arguments[1].marker;
				lat = marker.latLng.lat();
		    	lng = marker.latLng.lng();
			}

		    var R = 6371; // radius of earth in km
		    var closest = -1;
		    for(var i = 0; i < self.markers.length; i++ ) {
		        var mlat = self.markers[i].getPosition().lat();
		        var mlng = self.markers[i].getPosition().lng();
		        var dLat  = self.rad(mlat - lat);
		        var dLong = self.rad(mlng - lng);
		        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		            Math.cos(self.rad(lat)) * Math.cos(self.rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
		        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		        var d = R * c;
		        if(self.markers[i].OwnShop == false){
		        	d = (d + self.clientScore);
		        }
		        // self.distancesCollection[i] = {distance: d, id: self.markers[i].Id, OwnShop: self.markers[i].OwnShop};
		        // self.distances[i] = d;
		        if ( closest == -1 || d < self.distances[closest] ) {
		            closest = i;
		        }
		        self.markers[i].distance = d; // saving geolocation distance on marker
		    }

		    self.markers.sort(function(a, b) {
				return a.distance - b.distance;
			});
			var tmpMarkers = $.extend( true, [], self.markers );

			if(self.onlyClientShops == true){
				tmpMarkers = tmpMarkers.filter(function (obj) {
					return (obj.OwnShop == true);
				});
			}
			tmpMarkers.length = self.maxSearchResults;
			
			if(tmpMarkers != null){

				var origin1 = new google.maps.LatLng(lat, lng);
				var origin2 = "";
				var dist;

				var bounds = new google.maps.LatLngBounds();
				bounds.extend(origin1);
				for(var i = 0; i < tmpMarkers.length; i++){
					if(tmpMarkers[i].OwnShop){
						foundClient++;
					}
					bounds.extend(new google.maps.LatLng(tmpMarkers[i].getPosition().lat(), tmpMarkers[i].getPosition().lng()));
				}
				self.map.fitBounds(bounds);
				self.displayClosestStore(tmpMarkers, dist);
				if(foundClient > 0){
					self.showClientOnlyCheckbox();
				} else {
					self.hideClientOnlyCheckbox();
				}
			}
		}
	},





	// ---------------------------------------------------------------------
	// Opening hours related code
	// ---------------------------------------------------------------------
	openOpeningHours: function(parent, clkEvent){
		var self = this,
			loader = parent.find('.opening-hours-loader'),
			ohCon = parent.find('.opening-hours'),
			id = parent.find('.locator').attr('data-store-id');

		loader.addClass('loading');
		parent.addClass('opening-hours-shown');

		// service delay tests
		// setTimeout(function(){self.getOpeningHours(ohCon, loader, id);}, 1000);
		self.getOpeningHours(ohCon, loader, id);
	},
	openOpeningHoursFromMap: function(id){

		var self = this;
		var parent = self.storeSidebar.find('.result[data-store-id='+ id +']');
		var loader = parent.find('.opening-hours-loader'),
			ohCon = parent.find('.opening-hours');

		if(!ohCon.hasClass('loaded') && !ohCon.hasClass('shown')){
			loader.addClass('loading');
			parent.addClass('opening-hours-shown');

			// service delay tests
			// setTimeout(function(){self.getOpeningHours(ohCon, loader, id);}, 1000);
			self.getOpeningHours(ohCon, loader, id);
		}
		if(self.mobile){
			self.scrollToResultMobile(id);
		}
	},
	getOpeningHours: function(elm, loader, id){
		var self = this;

		var siteLocale = 'en_GB';
		
		if(window.location.href.indexOf('.net/se/') != -1 || window.location.href.indexOf('.se/') != -1) {
			siteLocale = 'sv_SE';
		} else if (window.location.href.indexOf('.dk/') != -1) {
			siteLocale = 'da_DK';
		}
		
		var endpoint = '/on/demandware.store/Sites-client-Site/' + siteLocale + '/Stores-OpeningHours?storeId=' + id;
		var openingAjax = $.ajax({
			url: endpoint,
			//url: '/static/tmp-opening-hour.json',
			contentType: "application/json; charset=utf-8",
            type: "GET",
            dataType: "json",
            success: function (data, status, e)
            {
                if (data.length > 0) {
                	self.formatOpeningHours(elm, loader, data);
                } else {
                	//loader.removeClass('loading');
                	// TODO - error messages
                    //console.log('fail data: ', data);
	                //console.log('fail status: ', status);
	                //console.log('fail e: ', e);
	                //self.serviceFailed();
                }
            },
            error: function (data, e, stack)
            {
            	//loader.removeClass('loading');
            	// TODO - error messages
                //console.log('fail data: ', data);
                //console.log('fail e: ', e);
                //console.log('fail stack: ', stack);
	            //self.serviceFailed();
            }
		});
	},
	formatOpeningHours: function(elm, loader, data){
		var self = this;

		elm.addClass('loaded shown');
		elm.find('.slider').append(self.formatSidebarOpeningHours(data));
		self.bindSidebarOpeningHoursNav(elm);
		loader.removeClass('loading');
		elm.find('a.headline').off('click').click(function(e){
			e.preventDefault();
			elm.removeClass('shown');
			self.bindSidebarOpeningHours();
		});
	},



	// ---------------------------------------------------------------------
	// SideBar related code
	// ---------------------------------------------------------------------
	displayClosestStore: function(storeMarkers, distance){
		this.resetShownStores();
		this.hideDefaultDesc();
		if(storeMarkers.length){
			this.shownMarkerIds = [];
			for(var j = 0; j < storeMarkers.length; j++ ){
				for(var i = 0; i < this.stores.length; i++ ){
					if(storeMarkers[j].id === this.stores[i].Id){
						this.shownMarkerIds.push(storeMarkers[j].id);
						this.formatClosestStore(this.stores[i], distance, false);
					}
				}
			}
		}
		// else {// Display recommended store, if any // }
	},
	displaySidebarMarker: function(storeId, distance){
		var self = this;

		if(storeId !== undefined && this.stores !== null){
			this.shownMarkerId = storeId;

			var tmpIds = [];
			if(self.shownMarkerIds.length){
				for(var t = 0; t < self.shownMarkerIds.length; t++){
					tmpIds.push(self.shownMarkerIds[t].Id);
				}
			}
			if ( $.inArray( storeId, self.shownMarkerIds ) > -1 || $.inArray( storeId, tmpIds ) > -1){
				var collection = self.shownMarkerIds.filter(function(obj) { return this.stores.indexOf(obj) == -1; });
				self.markerStoreCon.removeClass('is--active').hide().find('.inner').html('');
				self.storeSidebar.find('.result').each(function(elm, idx){
					var thiz = $(this);
					thiz.removeClass('selected');
					if(thiz.attr('data-store-id') == storeId){
						thiz.addClass('selected');
					}
				});
			} else {
				self.storeSidebar.find('.result').removeClass('selected');
				for(var i = 0; i < this.stores.length; i++ ){
					if(storeId === this.stores[i].Id){
						var store = this.stores[i],
							setDistanceLater = false,
							tmpPos = null;
						this.hideMarkerStore();
						this.formatMarkerStore(this.stores[i], setDistanceLater);
					}
				}
			}
		}
	},
	hideMarkerStore: function(){
		var inner = this.resultsConInner;
		if(this.markerStoreShown == true){
			this.resultsConInner.find('.result').eq(0).remove();
			this.resultsConInner.scrollTop(0);
			this.markerStoreShown = false;
		}
	},
	// setDistanceInClosestCon: function(distance){
	// 	var distanceCon = this.closestCon.find(".distance");

	// 	distanceCon.removeClass("loading");
	// 	distanceCon.html(distance);
	// },
	setDistanceInMarkerCon: function(distance){
		var distanceCon = this.markerStoreCon.find(".distance");

		distanceCon.removeClass("loading");
		distanceCon.html(distance);
	},
	loadMoreStores: function(){
		var self = this,
			foundClient = 0;

		var tmpMarkers = $.extend( true, [], self.markers );
		if(self.onlyClientShops == true){
			tmpMarkers = tmpMarkers.filter(function (obj) {
				return (obj.OwnShop == true);
			});
		}
		for(var j = 0; j < self.shownMarkerIds.length; j++){
			tmpMarkers.shift();
		}

		var count = self.shownMarkerIds.length + self.maxSearchResults;
		tmpMarkers.length = self.maxSearchResults;

		for(var j = 0; j < tmpMarkers.length; j++){
			for(var i = 0; i < self.stores.length; i++){
				if(tmpMarkers[j] !== undefined && self.stores[i] !== undefined){
					if(self.stores[i].Id == tmpMarkers[j].Id){
						if(self.stores[i].OwnShop == true){
							foundClient++;
						}
						self.shownMarkerIds.push(tmpMarkers[j].Id);
						self.formatClosestStore(self.stores[i], '', false);
					}
				}
				
			}
		}
		if(foundClient > 0){
			self.showClientOnlyCheckbox();
		} else {
			self.hideClientOnlyCheckbox();
		}
		self.bindScrollLoadMore();
	},
	loadMoreStoresNearest: function(){
		var self = this,
			foundClient = 0;

		var tmpStores = $.extend( true, [], self.stores );
		if(self.onlyClientShops == true){
			tmpStores = tmpStores.filter(function (obj) {
				return (obj.OwnShop == true);
			});
		}
		for(var j = 0; j < self.shownMarkerIds.length; j++){
			tmpStores.shift();
		}

		var count = self.shownMarkerIds.length + self.maxSearchResults;
		tmpStores.length = self.maxSearchResults;

		for(var j = 0; j < tmpStores.length; j++){
			for(var i = 0; i < self.stores.length; i++){
				if(tmpStores[j] !== undefined && self.stores[i] !== undefined){
					if(self.stores[i].Id == tmpStores[j].Id){
						if(self.stores[i].OwnShop == true){
							foundClient++;
						}
						self.shownMarkerIds.push(tmpStores[j].Id);
						//self.showNearestSearch(self.stores[i], '', false);
						self.formatClosestStore(self.stores[i], '', false);
					}
				}
				
			}
		}
		if(foundClient > 0){
			self.showClientOnlyCheckbox();
		} else {
			self.hideClientOnlyCheckbox();
		}
		self.bindScrollLoadMore();
	},
	formatLoadMore: function(){
		var self = this;

		if(self.storeSidebar.find('.load-more-con').length > 0){
			self.storeSidebar.find('.load-more-con').remove();
		}

		var tmpl = self.loadMoreTmpl;
		self.resultsConInner.append(tmpl);
		self.resultsConInner.find('.button-load-more').off('click').click(function(e){
			if(self.searchTerm == ""){
				self.loadMoreStores();
			} else {
				self.loadMoreStoresNearest();
			}
		});
	},
	bindScrollLoadMore: function(){
		var self = this;

		// function GetScrollerEndPoint()
		// {
		// 	var scrollHeight = self.resultsConInner.prop('scrollHeight');
		// 	var divHeight = self.resultsConInner.height();
		// 	var scrollerEndPoint = scrollHeight - divHeight;
			
		// 	var divScrollerTop =  self.resultsConInner.scrollTop();
		// 	if(divScrollerTop >= scrollerEndPoint)
		// 	{
		// 	   self.loadMoreStores();
		// 	}
		// }
		// self.resultsConInner.off('scroll').on('scroll', function(){
		// 	GetScrollerEndPoint();
		// });
		self.formatLoadMore();
	},
	formatClosestStore: function(store, distance, reset){
		var self = this;

		//store.setDistance = true;
		if(reset === true){
			self.resultsConInner.html('');
		}
		self.resultsConInner.append(self.formatSidebarStore(store));
		
		self.bindSidebarLocators();
		self.bindScrollLoadMore();
		if(store.OpeningHours == true && store.OwnShop == true){
			self.bindSidebarOpeningHours();
		}

		if(!self.resultsCon.hasClass('is--active')){
			self.resultsCon.addClass("is--active");
		}
	},
	formatMarkerStore: function(store, setDistance){
		var markerConInner = this.markerStoreCon.find(".inner"),
			content = '';

		this.markerStoreShown = true;
		if(setDistance){
			store.setDistance = true;
		}
		content = this.formatSidebarStore(store);
		if(!this.resultsCon.hasClass('is--active')){
			this.resultsCon.addClass('is--active');
		}
		this.resultsConInner.prepend(content);
		this.resultsConInner.find('.result:eq(0)').addClass('selected');
		this.resultsConInner.scrollTop(0);
		//this.markerStoreCon.show().addClass("is--active");
		//markerConInner.html(content);
		//markerConInner.find('.result').addClass('selected');
		this.bindSidebarLocators();
		this.hideDefaultDesc();
		if(store.OpeningHours == true && store.OwnShop == true){
			this.bindSidebarOpeningHours();
		}
	},
	newLocation: function(storeId, e){
		var self = this,
			marker = null;

		for(var i = 0; i < self.markers.length; i++ ) {
			if(self.markers[i].Id == storeId){
				marker = self.markers[i];
			}
			self.markers[i].info.close();
	    }
	    self.storeSidebar.find('.result').removeClass('selected');
	    $(e.currentTarget).closest('.result').addClass('selected');
		if(marker != null){
			marker.info.open(self.map, marker);
			google.maps.event.addListener(marker.info, 'domready', function() {
				var ooh = $('.open-opening-hours');
				ooh.off('click').on('click', function() {
					var id = $(this).attr('data-store-id');
					self.openOpeningHoursFromMap(id);
				});
			});
			if(self.mobile){
				// $('body').animate({
			 //        scrollTop: $('#primary').offset().top
			 //    }, 600);
				self.delayedSetMapCenterObj = {                       
					lat: marker.position.lat(),
					lng: marker.position.lng(),
					zoomLevel: 13
				};

				// timeout needed if the mobile keyboard is shown - otherwise calculating map height is obscured
				window.setTimeout(self.delayedSetMapCenter.bind(self), 100);
			} else {
				self.map.setCenter(new google.maps.LatLng(marker.position.lat(), marker.position.lng()));
				//self.map.setZoom(16);
			}
		}
	},
	delayedSetMapCenter: function(){
		var self = this;
		$(".mobile-open-map").trigger("click");
		this.map.setCenter(new google.maps.LatLng(self.delayedSetMapCenterObj.lat, self.delayedSetMapCenterObj.lng));
		this.map.setZoom(self.delayedSetMapCenterObj.zoomLevel);
	},

	



	// ---------------------------------------------------------------------
	// Generic format of sidebar stores from template
	// ---------------------------------------------------------------------
	formatSidebarStore: function(data){
		var tmpTmpl = this.storeTmpl;

		var strOwnShop = tmpTmpl.split('data-label-own="')[1];
		strOwnShop = strOwnShop.split('"')[0];

		var strOtherShop = tmpTmpl.split('data-label-other="')[1];
		strOtherShop = strOtherShop.split('"')[0];

		var tmpl = '<div class="result" data-store-id="'+ data.Id +'" data-own-store="'+ data.OwnShop +'">' + tmpTmpl + '</div>';
		tmpl = tmpl.replace('[AdressLine1]', data.AdressLine1);
		tmpl = tmpl.replace('[AdressLine2]', data.AdressLine2);
		tmpl = tmpl.replace('[AreaCode]', data.AreaCode);
		tmpl = tmpl.replace('[City]', data.City);
		tmpl = tmpl.replace('[Phone1]', data.Phone);
		tmpl = tmpl.replace('[Phone2]', data.Phone);
		tmpl = tmpl.replace('[OpeningHoursClass]', (data.OpeningHours == true && data.OwnShop == true ? 'has-opening-hours' : '' ));
		tmpl = tmpl.replace('[id]', data.Id);
		tmpl = tmpl.replace('[id1]', data.Id);
		tmpl = tmpl.replace('[OwnShop]', (data.OwnShop ? strOwnShop : strOtherShop));
		tmpl = tmpl.replace('[DirectionsUrl]', this.formatDirectionsUrl(data));
		tmpl = tmpl.replace('[ShopURL]', this.formatShopUrl(data.ShopURL));
		
		return tmpl;
	},
	formatSidebarOpeningHours: function(data){
		var tmpTmpl = this.openingHourTmpl,
			output = "";

		for(var d = 0; d < data.length; d++){
			var tmpl = '<div class="item">' + tmpTmpl + '</div>';
			tmpl = tmpl.replace('[OpeningHoursTitle]', data[d].title);
			for(var i = 0; i < data[d].days.length; i++){
				tmpl = tmpl.replace('[OpeningHours'+ (i+1) +']', data[d].days[i]);
			}
			output += tmpl;
		}

		return output;
	},




	// ---------------------------------------------------------------------
	// Search Box related code
	// ---------------------------------------------------------------------
	searchButtonClick: function(term, e){
		var self = this;
		this.searchTerm = term;
		setTimeout(function(){self.geocodeAddress(term);}, 250);
		//this.geocodeAddress(term);
	},
	geocodeAddress: function (term) {
		var self = this,
			geocoder = new google.maps.Geocoder();

		if(term.length > 1){
			if(this.defaultDescriptionCon.hasClass('shown')){
				this.defaultDescriptionCon.removeClass('shown');
			}
			
			
			//første resultat fra auto suggest
			var firstResult = $(".pac-container .pac-item:first").text();

			var country = $('#country').data().country;
			
			var geoLoc = $('#geoLoc').data();
			if (geoLoc != null){
				if (geoLoc.latitude != null && geoLoc.longitude != null){
					if (firstResult == ""){
						var request = {
								address: term,
								bounds: new google.maps.LatLngBounds(
										new google.maps.LatLng({lat: geoLoc.latitude, lng: geoLoc.longitude}),
										new google.maps.LatLng({lat: geoLoc.latitude, lng: geoLoc.longitude}))
						}
					}
					else{
						var request = {
								//Bruger første auto suggest i stedet for det der står i textboksen (term)
								address: firstResult,
								bounds: new google.maps.LatLngBounds(
										new google.maps.LatLng({lat: geoLoc.latitude, lng: geoLoc.longitude}),
										new google.maps.LatLng({lat: geoLoc.latitude, lng: geoLoc.longitude}))
						};
					}
				}
			}
			geocoder.geocode(request, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					if(self.searchMarker !== null){
						self.searchMarker.setMap(null);
					}
					if(typeof results[0].geometry.location === "object"){
						self.map.setCenter(results[0].geometry.location);
						self.map.setZoom(11);

						var markerIcon = new google.maps.MarkerImage(StoreLocatorIcons.mapMarkerSearch2x, null, null, null, new google.maps.Size(25,32));
						self.searchMarker = new google.maps.Marker({
							map: self.map,
							position: results[0].geometry.location,
							icon: markerIcon,
							draggable: false,
							title: "Placering af din søging"
						});
						self.searchNearestStoreLatLng = results[0].geometry.location;
						self.showNearestSearch(results[0].geometry.location);
					} else {
						//self.resetSearchResults(false);
					}
				} else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
					//ZERO_RESULTS
				} else {
					// error
					throw new Error('Geocode was not successful for the following reason: ' + status);					
				}
			});
		} else {
			//self.resetSearchResults(true);
		}
	},
	showNearestSearch: function(location){
		var self = this,
			foundClient = 0,
			searchDistances = [],
			tmpSearchDistances = [],
			lat = (typeof location.lat == "function" ? location.lat() : location.Latitude),
			lng = (typeof location.lng == "function" ? location.lng() : location.Longitude),
			R = 6371; // radius of earth in km

		self.resetShownStores();
		self.onlyClientShops = false;
		$('.client-filter-con input').removeAttr('checked');
	    for(var i = 0; i < self.stores.length; i++ ) {
	    	var mlat = self.stores[i].Latitude,
	    		mlng = self.stores[i].Longitude,
	    		dLat  = self.rad(mlat - lat),
	    		dLong = self.rad(mlng - lng);
	        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	            Math.cos(self.rad(lat)) * Math.cos(self.rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
	        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	        var d = R * c;
	        if(self.stores[i].OwnShop == false){
	        	d = (d + self.clientScore);
	        }
	        self.stores[i].searchDistance = d;
	    }


	    self.stores.sort(function(a, b) {
			return a.searchDistance - b.searchDistance;
		});
		var tmpStores = $.extend( true, [], self.stores );
		if(self.onlyClientShops == true){
			tmpStores = tmpStores.filter(function (obj) {
				return (obj.OwnShop == true);
			});
		}

		tmpStores.length = self.maxSearchResults;
		
		if(tmpStores != null){

			var origin1 = new google.maps.LatLng(lat, lng);
			var origin2 = "";
			var dist;

			var bounds = new google.maps.LatLngBounds();
			bounds.extend(origin1);
			for(var i = 0; i < tmpStores.length; i++){
				if(tmpStores[i].OwnShop){
					foundClient++;
				}
				bounds.extend(new google.maps.LatLng(tmpStores[i].Latitude, tmpStores[i].Longitude));
			}
			self.map.fitBounds(bounds);
			self.displayClosestStore(tmpStores, dist);

			if(foundClient > 0){
				self.showClientOnlyCheckbox();
			} else {
				self.hideClientOnlyCheckbox();
			}
		}
		self.preFormatSearchResults(tmpStores);

	},
	resetShownStores: function(){
		var self = this;

		self.shownMarkerIds = [];

		self.resultsConInner.html('').scrollTop(0);
		self.markerStoreCon.hide().removeClass('is--active').find('.inner').html('');
		self.resultsCon.removeClass("is--active");
	},
	resetSearch: function(){
		var self = this;

		self.searchInput.val('');
		self.searchTerm = '';
	},
	preFormatSearchResults: function(results){
		results.sort(function(a, b) {
			return a.distanceKmNumber - b.distanceKmNumber;
		});
		this.formatResults(results);
	},
	formatResults: function(results){
		var self = this,
			content = '';

		this.resetResults();

		for(var i = 0; i < results.length; i++){
			var tmpl = self.formatSidebarStore(results[i]);
			self.resultsConInner.append(tmpl);
		}

		if(!this.resultsCon.hasClass("is--active")){
			this.resultsCon.addClass("is--active");
		}
		// TODO - this filtering should be done in showNearestSearch
		// if(self.onlyClientShops){
		// 	self.formatOnlyClientStores();
		// }
		self.bindSidebarLocators();
		self.bindSidebarOpeningHours();
		self.bindScrollLoadMore();
	},
	resetResults: function(){
		this.resultsConInner.html("");
	},
	formatOnlyClientStores: function(){
		var self = this;
		var results = self.storeSidebar.find(".result").removeClass('selected'),
			found = 0,
			warning = $('.no-stores-warning');
		results.each(function(idx, elm){
			if(self.onlyClientShops == true){
				if($(elm).attr('data-own-store') == "false"){
					$(elm).hide();
				} else {
					found++;
				}
			} else {
				found++;
				warning.hide();
				$(elm).show();
			}
		});
		if(found == 0){
			warning.show();
		}
	},
	showNoResults: function(){
		if(!this.noResultsCon.hasClass("is--active")){
			this.noResultsCon.addClass("is--active");
		}
		var content = this.noResultsContent.replace("[[search_term]]", this.term);
		this.noResultsCon.html(content);
	},
	hideNoResults: function(){
		if(this.noResultsCon.hasClass("is--active")){
			this.noResultsCon.removeClass("is--active");
		}
	}
}

$(document).ready(function(){
	var storeLocatorCon = $('.js--store-locator');
	Log.db("doc ready",storeLocatorCon)
	if(storeLocatorCon.length == 1){
		StoreLocator.init();
	}
});