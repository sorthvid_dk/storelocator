# Store Locator #
Store Locator is a Google Maps based store locator module, and it harnesses the raw untamed POWER of KernelJS.

## Documentation ##
See [documentation](/documentation/js) for the project

## Browser support ##
Internet Explorer 10+
Chrome latest
Safari latest
Firefox latest
Opera latest

## Changelog ##
* 1.0.0: first version 


## TODO ##